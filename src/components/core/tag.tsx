import React from 'react';
import {Box, Spacing, Text, Border, Queue} from '../../components';
import { CheckIcon, CrossIcon, DangerIcon, WarnIcon } from '../icons'

const tagIcon: any = {
  Check: CheckIcon,
  Cross: CrossIcon,
  Danger: DangerIcon,
  Warning: WarnIcon,
};

const statusOpacity = {
  default: 1,
  disabled: 0.5,
  active: 0.7,
};

const helperGetBorderColor = (category, type) => category === 'default' ? 'primary400' : category === 'rounded' ? 'primary300' : type === 'warning' ? 'caution500' : type === 'danger' ? 'destructive500' : type === 'success' ? 'success500' : 'primary500';

const helperGetBackgroundColor = (category, type) => category === 'default' ? 'primary100' : category === 'rounded' ? 'primary100' : type === 'warning' ? 'caution200' : type === 'danger' ? 'destructive200' : type === 'success' ? 'success200' : 'primary100';


export const Tag: React.FC<TagType> = ({
  category = 'default',
  type = 'unremoveable',
  avatar,
  children,
}) => {
  return (
      
      <Box height={30} width={'auto'} alignSelf={'flex-start'}>
        <Border role={helperGetBorderColor(category, type)} radius={category === 'rounded' ? 1000 : 4} lineWidth={1} grow={1}>
          <Box role={helperGetBackgroundColor(category, type)}>
            <Spacing pv={1} ph={2}>
                <Text
                      fontFamily={'Montserrat'}
                      role={'primary500'}
                      type={'callout'}
                      textAlign={'center'}
                      >
                  {children}
                </Text>
            </Spacing>
            </Box>
        </Border>
      </Box>
  );
};

type TagType = {
  category?: 'default' | 'message' | 'rounded';
  type?: 'removeable' | 'unremoveable' | 'warning' | 'danger' | 'success' | 'user';
  avatar?: boolean;
  children?: JSX.Element | JSX.Element[] | string;
};