import React from 'react';
import { View } from 'react-native';
import { Border, Box, Spacing } from '../';
import { InputIcon } from '../icons/input-icon';

type InputMessageType = {
    role: 'default' | 'warning' | 'error' | 'success';
    children?: JSX.Element | JSX.Element[] | string;
};

export const InputMessage: React.FC<InputMessageType> = ({ role, children }) => {
    const roleToColor = {
        'default': 'primary100',
        'warning': 'caution200',
        'error': 'destructive200',
        'success': 'success200'
    }

    const InfoIcon = () => {
        return (
            <View style={{ marginRight: 12 }}>
                <InputIcon role={roleToColor[role]} />
            </View>
        )
    }

    return (
        <Border radius={4}>
            <Box role={roleToColor[role]} height={48} width='97%'>
                <Spacing pl={3.5} pv={2}>
                    <Box justifyContent='flex-start' alignItems='center' flexDirection='row' width={'80%'}>
                        <InfoIcon />
                        {children}
                    </Box>
                </Spacing>
            </Box>
        </Border>
    )
}