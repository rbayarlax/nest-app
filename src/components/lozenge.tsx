import React, { useState } from 'react';
import { Text, View, Dimensions, StyleSheet } from 'react-native';
import styled from 'styled-components/native';

type ItemStatus = {
    type: 'default' | 'success' | 'pending' | 'error';
    style: 'bold' | 'subtle';
    id: any;
    text: any;
    width: number;
};
export const Lozenge: React.FC<ItemStatus> = ({ type, style, id, text, width }) => {
    const [color, setColor] = useState('null');
    const [bg, setBg] = useState('white');
    // const fullwidth = Dimensions.get('window').width;
    React.useEffect(() => {
        if (style === 'subtle') {
            switch (type) {
                case 'default': setBg('#E6EBF2'), setColor('#172b4d'); break;
                case 'success': setBg('#E3F1DE'), setColor('#0F8043'); break;
                case 'pending': setBg('#FCF0CD'), setColor('#9C6F19'); break;
                case 'error': setBg('#BF0A12'), setColor('#FBEAE5'); break;
            }
        } else {
            switch (type) {
                case 'default': setBg('#172B4D'), setColor('#F7FBFF'); break;
                case 'success': setBg('#4FB83D'), setColor('#F7FBFF'); break;
                case 'pending': setBg('#EDC200'), setColor('#172B4D'); break;
                case 'error': setBg('#DF3617'), setColor('#F7FBFF'); break;
            }
        }
    }, [style, type])
    const styles = StyleSheet.create({
        Box: {
            backgroundColor: bg,
            height: 24,
            fontSize: 12,
            borderRadius: 4,
            lineHeight: 16,
            paddingHorizontal: 8,
            paddingVertical: 4,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center'
        },
        Container: {
            width: width,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingLeft: 16,
            paddingRight: 16,
        }
    });
    return (
        <View style={styles.Container}>
            <View style={styles.Box}>
                <Text style={{ color: color, fontSize: 12 }}>
                    {text}
                </Text>
            </View>
            <Text style={{ fontSize: 15, lineHeight: 20, color: 'grey', textAlign: 'center' }}>
                {id}
            </Text>
        </View>
    );
};