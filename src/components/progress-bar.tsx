import React, { useRef, useEffect } from 'react';
import { Box, Spacing } from './layout/index';
import { Border } from './core/index'
import { Platform, StyleSheet, Text, View, Animated } from 'react-native';
import { ColorType } from '../components/types';


export const ProgressBar: React.FC<ProgressType> = ({ curProgress=0, endProgress=100, duration=5000, role, backgroundRole, width, height='4' }) => {
    const progress = useRef(new Animated.Value(curProgress ? curProgress : 0)).current
    useEffect(() => {
        Animated.timing(
            progress,
            {
                toValue: endProgress,
                duration: duration,
                useNativeDriver: false
            }
        ).start();
        console.log(curProgress)
        console.log(endProgress)
    }, [progress])
    
    return (
        // <ProgressBar color={color} backgroundColor={backgroundColor} progress={progress} width={width ? width : '100%'} height={height}></ProgressBar>
        // <Box>
        //     <Animated.View
        //         style={{
        //             width: progress,
        //         }}
        //     >

        //     </Animated.View>
        // </Box>
        // <View style={{backgroundColor: 'red', flex: 1, height: 4, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
        //     <View></View>
        // </View>

        <Box flex={1} height={4}>
            <Border radius={4}>
                <Box height={4} role={backgroundRole}>
                    <Border radius={4}>
                        <Animated.View style={{ height: 4, backgroundColor: '#172B4D', 
                        width: progress.interpolate({
                            inputRange: [0, 100],
                            outputRange: ['0%', '100%']
                        })}}>

                        </Animated.View>
                    </Border>
                </Box>
            </Border>
        </Box>
    );
};

type ProgressType = {
    width?: number | string;
    height?: number | string;
    role?: ColorType;
    backgroundRole?: ColorType;
    curProgress?: number;
    endProgress?: number;
    progress?: number;
    duration?: number;
};