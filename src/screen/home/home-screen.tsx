import React from 'react';
import { SafeAreaView } from 'react-native';
import { Spacing, Text } from '../../components';

export const HomeScreen = () => {
    return (
        <SafeAreaView style={{ backgroundColor: '#FFFFFF' }}>
            <Spacing p={5}>
                <Text bold type={'headline'}>Welcome to Nest APP</Text>
            </Spacing>
        </SafeAreaView>
    );
};
