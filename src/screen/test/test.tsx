import React from 'react';
import { SafeAreaView, ScrollView } from 'react-native';
import { Button } from '../../components/core';
import { Stack, Queue, Box, Text, Spacing, ProgressBar } from '../../components';
import { Input, InputWithHelperTextAndCounter, InputWithMessage, InputAllInOne } from '../../components/input'
import { EyeIcon, KeyIcon } from '../../components/icons';
import { LoadingCircle } from '../../components/layout/loading-circle';
import { Tag } from '../../components/core/tag';

export const TestScreen = () => {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Spacing p={5}>
        <ScrollView>
          <Stack size={5}>
            <Box flex={1} justifyContent='center'>
              <Stack size={2}>
                <Text type={'title1'} bold underline>Typography</Text>
                <Text type={'title1'}>Нэг түмэн инженер</Text>
                <Text type={'title2'}>Нэг түмэн инженер</Text>
                <Text type={'title3'}>Нэг түмэн инженер</Text>
                <Text type={'headline'}>Нэг түмэн инженер</Text>
                <Text type={'body'}>Нэг түмэн инженер</Text>
                <Text type={'callout'}>Нэг түмэн инженер</Text>
                <Text type={'subheading'}>Нэг түмэн инженер</Text>
                <Text type={'caption1'}>Нэг түмэн инженер</Text>
                <Text type={'caption2'}>Нэг түмэн инженер</Text>
                <Text numberOfLines={4} type={'body'}>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi
                  eros dui, pellentesque eget condimentum sed, interdum non lectus.
                  Proin elementum risus elit, eget tempor lorem viverra vitae
              </Text>
              </Stack>
            </Box>

            <Box flex={1}>
              <Stack width={'100%'} size={4}>
                <Text type={'title1'} bold underline>Buttons</Text>
                <Button onPress={() => console.log('handled')} size="s">
                  <Text
                    fontFamily={'Montserrat'}
                    role={'white'}
                    type={'callout'}
                    textAlign={'center'}
                    bold>
                    Товчлуур
                </Text>
                </Button>
                <Button
                  onPress={() => console.log('handled')}
                  status="disabled">
                  <Text
                    fontFamily={'Montserrat'}
                    role={'white'}
                    type={'callout'}
                    textAlign={'center'}
                    bold>
                    Товчлуур
                </Text>
                </Button>
                <Button
                  onPress={() => console.log('handled')}
                  status="active"
                  type="destructive">
                  <Text
                    fontFamily={'Montserrat'}
                    role={'white'}
                    type={'callout'}
                    textAlign={'center'}
                    bold>
                    Товчлуур
                </Text>
                </Button>
                <Button
                  onPress={() => console.log('handled')}
                  category="ghost"
                  type="destructive">
                  <Text
                    fontFamily={'Montserrat'}
                    role={'destructive500'}
                    type={'callout'}
                    textAlign={'center'}
                    bold>
                    Товчлуур
                </Text>
                </Button>
                <Button
                  onPress={() => console.log('handled')}
                  width={90}
                  category="text">
                  <Text
                    fontFamily={'Montserrat'}
                    role={'primary500'}
                    type={'callout'}
                    textAlign={'center'}
                    bold>
                    Товчлуур
                </Text>
                </Button>
                <Button
                  onPress={() => console.log('handled')}
                  category="text"
                  type="destructive"
                  status="active"
                  width={90}>
                  <Text
                    fontFamily={'Montserrat'}
                    role={'destructive500'}
                    type={'callout'}
                    textAlign={'center'}
                    bold>
                    Товчлуур
                </Text>
                </Button>
              </Stack>
            </Box>


            <Box flex={1} justifyContent='center'>
              <Stack size={2}>
                <Text type={'title1'} bold underline>Inputs</Text>
                <Input
                  placeholder={'Овог Нэр'}
                  type={'default'}
                  onChangeText={(text: string) => console.log(text)}
                  onSubmitEditing={() => console.log('nani')}
                />
                <Input
                  placeholder={'Овог Нэр'}
                  type={'default'}
                  LeftIcon={EyeIcon}
                  onChangeText={(text: string) => console.log(text)}
                  onSubmitEditing={() => console.log('nani')}
                />
                <Input
                  placeholder={'Овог Нэр'}
                  type={'default'}
                  RigthIcon={KeyIcon}
                  onChangeText={(text: string) => console.log(text)}
                  onSubmitEditing={() => console.log('nani')}
                />
                <Input
                  placeholder={'Овог Нэр'}
                  type={'password'}
                  onChangeText={(text: string) => console.log(text)}
                  onSubmitEditing={() => console.log('nani')}
                />
                <InputWithHelperTextAndCounter
                  placeholder={'Яаралтай үед холбоо барих'}
                  counter={true}
                  helperText={'Туслах текст '}
                  onChangeText={(text: string) => console.log(text)}
                  onSubmitEditing={() => console.log('nani')}
                />
                <InputWithMessage
                  placeholder={'Овог Нэр nani'}
                  messageText={'This is message'}
                  messageType={'default'}
                  onChangeText={(text: string) => console.log(text)}
                  onSubmitEditing={() => console.log('nani')}
                />
                <InputAllInOne
                  placeholder={'Овог Нэр nani'}
                  messageText={'This is message'}
                  messageType={'success'}
                  counter={true}
                  helperText={'Туслах текст '}
                  onChangeText={(text: string) => console.log(text)}
                  onSubmitEditing={() => console.log('nani')}
                />
              </Stack>
            </Box>

            <Box flex={1}>
              <Stack size={2}>
                <Text type={'title1'} bold underline>Tags</Text>
                <Box flex={1} flexDirection={'row'} justifyContent={'space-between'} flexWrap={'wrap'}>
                  <Tag >Tag</Tag>
                  <Tag category={'message'} type={'success'} >Success</Tag>
                  <Tag category={'message'} type={'danger'} >Danger</Tag>
                  <Tag category={'message'} type={'warning'} >Warning</Tag>
                  <Tag category={'rounded'} type={'user'} >Бямбадорж</Tag>
                </Box>
                
              </Stack>
            </Box>

            <Box flex={1}>
              <Stack size={2}>
                <Text type={'title1'} bold underline>Spinners</Text>
                <Box flex={1} flexDirection={'row'} justifyContent={'space-evenly'} flexWrap={'wrap'}>
                  <LoadingCircle width={32} height={32} />
                  <LoadingCircle width={32} height={32} />
                </Box>
              </Stack>
            </Box>

            <Box flex={1}>
              <Stack size={2}>
                <Text type={'title1'} bold underline>Progress Bar</Text>
                <ProgressBar backgroundRole='primary300' role='primary500' endProgress={100}></ProgressBar>
              </Stack>
            </Box>

          </Stack>
        </ScrollView>
      </Spacing>
    </SafeAreaView>
  );
};
